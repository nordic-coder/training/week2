module gitlab.com/nordic-coder/training/week2

go 1.12

require (
	g.ghn.vn/online/tenant v0.0.0-20190411035200-6e439a85678f // indirect
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/urfave/cli v1.20.0
	golang.org/x/crypto v0.0.0-20190424203555-c05e17bb3b2d // indirect
	golang.org/x/net v0.0.0-20190424112056-4829fb13d2c6 // indirect
	golang.org/x/sys v0.0.0-20190425045458-9f0b1ff7b46a // indirect
	golang.org/x/text v0.3.1 // indirect
	golang.org/x/tools v0.0.0-20190425001055-9e44c1c40307 // indirect
)
