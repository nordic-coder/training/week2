package gopanic

import (
	"log"
)

func Two() (err error) {
	for i := 1; i < 10; i++ {
		log.Println(i)
		checkMod(i)
	}
	return
}

func checkMod(i int) {
	if i%7 == 0 {
		panic(i)
	}
}
