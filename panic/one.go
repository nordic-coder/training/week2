package gopanic

import (
	"log"
)

func One() (err error) {
	for i := 1; i < 10; i++ {
		log.Println(i)
		if i%7 == 0 {
			panic(i)
		}
	}
	return
}
