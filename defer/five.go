package godefer

import "log"

func Five() (err error) {
	i := c()
	log.Println(i)
	return
}

func c() (i int) {

	defer log.Println(i)
	defer func(j int) {
		j++
	}(i)

	defer func() {
		i++
	}()

	return 1
}
