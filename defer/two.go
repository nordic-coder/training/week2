package godefer

import (
	"io"
	"log"
	"os"
)

func Two() (err error) {
	w, err := copyFile2("test.txt", "test2.txt")
	if err != nil {
		return
	}
	log.Println(w)
	return
}

func copyFile2(srcName, dstName string) (written int64, err error) {
	src, err := os.Open(srcName)
	if err != nil {
		return
	}
	defer src.Close()

	dst, err := os.Create(dstName)
	if err != nil {
		return
	}
	defer dst.Close()

	return io.Copy(dst, src)
}
