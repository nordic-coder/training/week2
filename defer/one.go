package godefer

//LIFO

import (
	"io"
	"log"
	"os"
)

func One() (err error) {
	w, err := copyFile("test.txt", "test2.txt")
	if err != nil {
		return
	}
	log.Println(w)
	return
}

func copyFile(srcName, dstName string) (written int64, err error) {
	src, err := os.Open(srcName)
	if err != nil {
		src.Close()
		log.Println("return")
		return
	}

	dst, err := os.Create(dstName)
	if err != nil {
		dst.Close()
		return
	}

	written, err = io.Copy(dst, src)

	src.Close()
	return
}
