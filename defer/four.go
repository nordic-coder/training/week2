package godefer

import (
	"fmt"
)

func Four() (err error) {
	for i := 0; i < 4; i++ {
		defer fmt.Println(i)
	}

	return
}
