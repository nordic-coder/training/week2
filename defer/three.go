package godefer

import (
	"fmt"
)

func Three() (err error) {
	i := 0
	defer func() {
		fmt.Println("---1---", i)
	}()
	defer func(i int) {
		fmt.Println("---2---", i)
	}(i)
	defer fmt.Println("---3---", i)
	i++
	return
}
