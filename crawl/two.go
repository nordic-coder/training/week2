package crawl

import (
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
)

var (
	hostname = "http://genk.vn"
	cache2   map[string]bool
)

func init() {
	cache2 = make(map[string]bool)
}

func Two() (err error) {
	ExampleScrape("https://vnexpress.net/thoi-su/bo-ngoai-giao-noi-ve-suc-khoe-cua-tong-bi-thu-3914885.html")
	return
}

func ExampleScrape(link string) {

	if _, ok := cache2[link]; !ok {
		cache2[link] = true
	} else {
		return
	}

	// Request the HTML page.
	res, err := http.Get(link)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(doc.Find(".title_news_detail").Text())

	// Find the review items
	doc.Find(".list_news").Each(func(i int, s *goquery.Selection) {
		link, _ := s.Find("a").Attr("href")
		ExampleScrape(link)
	})
}
