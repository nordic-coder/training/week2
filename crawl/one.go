package crawl

import (
  "fmt"
  "time"
	"log"
  "net/http"
  "sync"

	"github.com/PuerkitoBio/goquery"
)

var (
	domain   = ""
  initLink = "https://vnexpress.net/phap-luat/nhung-bat-thuong-dem-3-nguoi-trong-gia-dinh-bi-giet-o-binh-duong-3914210.html"
  cache map[string]bool
  cacheMutex sync.Mutex
)

func init() {
  cache = make(map[string]bool)
}

// One ..
func One() (err error) {
  crawl(initLink)

  log.Println("done")
  time.Sleep(time.Minute * 10)
	return
}

func isExists(link string) (result bool) {
  cacheMutex.Lock()
  if _,ok:=cache[link]; ok {
    result = true
  }
  cacheMutex.Unlock()
  return
}

func markExists(link string) {
  cacheMutex.Lock()
  cache[link] = true
  cacheMutex.Unlock()

}

func crawl(link string) {

  if isExists(link) || link == "" {
    return
  }

  markExists(link)

	res, err := http.Get(fmt.Sprintf("%v%v", domain, link))
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
  }
  
  // Lấy nội dung
  title := doc.Find("h1.title_news_detail").Text()

  fmt.Println(title)


	// Tìm các bài liên quan
	doc.Find(".list_title li").Each(func(i int, s *goquery.Selection) {
		link, _ := s.Find("a").Attr("href")
		go crawl(link)
  })

  doc.Find(".list_news").Each(func(i int, s *goquery.Selection) {
		link, _ := s.Find(".title_news a").Attr("href")
		go crawl(link)
  })

}
