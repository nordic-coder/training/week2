package gorecover

import (
	"fmt"
	"log"
)

func One() (err error) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()
	for i := 1; i < 10; i++ {
		log.Println(i)
		checkMod(i)
	}
	return
}

func checkMod(i int) {
	if i%7 == 0 {
		panic("i chia hết cho 7")
	}
}
