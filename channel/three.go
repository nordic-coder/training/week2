package channel

import "log"

// Three : Unbuffered Channel
func Three() (err error) {
	var ch = make(chan int)

	go func() {
		ch <- 100
	}()

	go func() {
		ch <- 200
	}()

	go func() {
		ch <- 300
	}()

	go func() {
		ch <- 400
	}()

	log.Println(<-ch)
	log.Println(<-ch)
	return
}
