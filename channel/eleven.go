package channel

import (
	"log"
)

var ch11 = make(chan int)

// Eleven : Buffered Channel
func Eleven() (err error) {

	arr := []int{4, 5, 3, 3, 5, 6, 7, 8, 4, 3, 2, 10}
	go test(arr)

	for i := range ch11 {
		log.Println(i)
	}

	return
}

func test(arr []int) {
	if len(arr) > 0 {
		max := arr[0]
		index := 0
		for k, v := range arr {
			if v > max {
				max = v
				index = k
			}
		}
		newArr := arr[:index]
		newArr = append(newArr, arr[index+1:]...)
		ch11 <- max
		// time.Sleep(1 * time.Second)
		test(newArr)
	} else {
	}
	return
}
