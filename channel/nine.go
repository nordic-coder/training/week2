package channel

import (
	"log"
)

// Nine : Buffered Channel
func Nine() (err error) {
	var ch = make(chan int, 2)

	ch <- 100
	ch <- 200

	close(ch)

	log.Println(<-ch)
	log.Println(<-ch)
	log.Println(<-ch)
	return
}
