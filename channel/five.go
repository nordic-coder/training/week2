package channel

import "log"

// Five : Buffered Channel
func Five() (err error) {
	var ch = make(chan int, 2)

	ch <- 100
	ch <- 200
	ch <- 300

	log.Println(<-ch)
	log.Println(<-ch)
	log.Println(<-ch)
	return
}
