package channel

import (
	"log"
)

// Twelve : Buffered Channel
func Twelve() (err error) {

	arr := []int{4, 5, 3, 3, 5, 6, 7, 8, 4, 3, 2, 10}
	test2(arr)

	for i := range ch11 {
		log.Println(i)
	}

	return
}

func test2(arr []int) {
	if len(arr) > 0 {
		max := arr[0]
		index := 0
		for k, v := range arr {
			if v > max {
				max = v
				index = k
			}
		}
		newArr := arr[:index]
		newArr = append(newArr, arr[index+1:]...)
		ch11 <- max
		// time.Sleep(1 * time.Second)
		test2(newArr)
	} else {
		close(ch11)
	}
	return
}
