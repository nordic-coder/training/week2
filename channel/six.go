package channel

import "log"

// Six : Buffered Channel
func Six() (err error) {
	var ch = make(chan int, 2)

	ch <- 100
	ch <- 200
	ch <- 200

	log.Println(<-ch)
	log.Println(<-ch)
	return
}
