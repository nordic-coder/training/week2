package channel

import "log"

// Ten : UnBuffered Channel
func Ten() (err error) {
	var ch = make(chan int)

	go func() {
		for i := 0; i < 10; i++ {
			ch <- i
		}
		// close(ch)
	}()

	for i := range ch {
		log.Println(i)
	}

	return
}
