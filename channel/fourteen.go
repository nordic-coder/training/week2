package channel

import (
	"log"
	"sync"
)

var cache2 = make(map[int]int)

var wg sync.WaitGroup

// Fourteen : Buffered Channel
func Fourteen() (err error) {

	for j := 0; j < 10; j++ {
		wg.Add(1)
		go func() {
			arr := []int{4, 5, 3, 3, 5, 6, 7, 8, 4, 3, 2, 10}
			for _, i := range arr {
				check2(i)
			}
			wg.Done()
		}()
	}

	wg.Wait()

	log.Println(cache2)

	return
}

var mutex2 sync.Mutex

func check2(i int) {
	mutex2.Lock()
	cache2[i]++
	mutex2.Unlock()
}
