package channel

import "log"
import "runtime"

// Two : Unbuffered Channel
func Two() (err error) {
	var ch = make(chan int)

	go func() {
		ch <- 100
	}()

	log.Println(<-ch)

	log.Println(runtime.NumGoroutine())
	log.Println(<-ch)
	return
}
