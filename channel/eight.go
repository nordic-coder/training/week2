package channel

import (
	"log"
	"time"
)

// Eight : Buffered Channel
func Eight() (err error) {
	var ch = make(chan int, 2)

	go func() {
		log.Println(<-ch)
		log.Println(<-ch)
	}()

	ch <- 100
	ch <- 200
	ch <- 300

	time.Sleep(10 * time.Second)

	return
}
