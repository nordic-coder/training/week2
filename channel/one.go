package channel

import "log"

// One : Unbuffered Channel
func One() (err error) {
	var ch = make(chan int)
	go func() {
		ch <- 100
	}()

	log.Println(<-ch)
	return
}

// có phải hên ko? lúc nào cũng in ra 100? lúc nào func goroutine kia cũng chạy xong trước cái main?
