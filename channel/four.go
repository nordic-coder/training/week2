package channel

import "log"

// Four : Buffered Channel => sai
func Four() (err error) {
	var ch = make(chan int)

	ch <- 100
	log.Println(<-ch)
	log.Println("test")
	ch <- 200

	log.Println(<-ch)
	return
}
