package channel

import (
	"log"
	"sync"
	"time"
)

var cache3 = make(map[int]int)

var wg15 sync.WaitGroup

// Fifteen : Buffered Channel
func Fifteen() (err error) {

	start := time.Now()
	for j := 0; j < 10; j++ {
		wg15.Add(1)
		go func() {
			arr := []int{4, 5, 3, 3, 5, 6, 7, 8, 4, 3, 2, 10}
			for _, i := range arr {
				crawl2(i)
			}
			wg15.Done()
		}()
	}

	wg15.Wait()

	time := time.Since(start)

	log.Println(time)

	log.Println(cache3)

	return
}

var mutex4 sync.Mutex

func crawl2(i int) {
	time.Sleep(10 * time.Millisecond)
	mutex4.Lock()
	cache3[i]++
	mutex4.Unlock()
}
