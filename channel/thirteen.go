package channel

import (
	"log"
	"sync"
)

var cache = make(map[int]int)

var wg13 sync.WaitGroup

func Thirteen() (err error) {

	for j := 0; j < 10; j++ {
		go func() {
			arr := []int{4, 5, 3, 3, 5, 6, 7, 8, 4, 3, 2, 10}
			for _, i := range arr {
				check(i)
			}
		}()
	}

	log.Println(cache)

	return
}

var mutex3 sync.Mutex

func check(i int) {
	mutex3.Lock()
	cache[i]++
	mutex3.Unlock()
}

// var cache = make(map[int]int)

// // Thirteen : Buffered Channel
// func Thirteen() (err error) {

// 	for j := 0; j < 100; j++ {
// 		go func() {
// 			arr := []int{4, 5, 3, 3, 5, 6, 7, 8, 4, 3, 2, 10}
// 			for _, i := range arr {
// 				check(i)
// 			}
// 		}()
// 	}

// 	log.Println(cache)

// 	return
// }

// var mutex sync.Mutex

// func check(i int) {
// 	mutex.Lock()
// 	cache[i]++
// 	mutex.Unlock()
// }
