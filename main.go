package main

import (
	"log"
	"os"
	"sort"

	"github.com/urfave/cli"
	"gitlab.com/nordic-coder/training/week2/channel"
	"gitlab.com/nordic-coder/training/week2/crawl"
	"gitlab.com/nordic-coder/training/week2/db"
	godefer "gitlab.com/nordic-coder/training/week2/defer"
	"gitlab.com/nordic-coder/training/week2/goroutine"
	gopanic "gitlab.com/nordic-coder/training/week2/panic"
	gorecover "gitlab.com/nordic-coder/training/week2/recover"
	gosummay "gitlab.com/nordic-coder/training/week2/summary"
)

func main() {
	app := cli.NewApp()
	app.Version = "1.2.3"
	app.Commands = []cli.Command{
		{
			Name: "goroutine-1",
			Action: func(c *cli.Context) (err error) {
				err = goroutine.One()
				return
			},
		},
		{
			Name: "goroutine-2",
			Action: func(c *cli.Context) (err error) {
				err = goroutine.Two()
				return
			},
		},
		{
			Name: "goroutine-3",
			Action: func(c *cli.Context) (err error) {
				err = goroutine.Three()
				return
			},
		},
		{
			Name: "goroutine-4",
			Action: func(c *cli.Context) (err error) {
				err = goroutine.Four()
				return
			},
		},
		{
			Name: "goroutine-5",
			Action: func(c *cli.Context) (err error) {
				err = goroutine.Five()
				return
			},
		},
		{
			Name: "goroutine-6",
			Action: func(c *cli.Context) (err error) {
				err = goroutine.Six()
				return
			},
		},
		{
			Name: "goroutine-7",
			Action: func(c *cli.Context) (err error) {
				err = goroutine.Seven()
				return
			},
		},
		{
			Name: "defer-1",
			Action: func(c *cli.Context) (err error) {
				err = godefer.One()
				log.Println(err)
				return
			},
		},
		{
			Name: "defer-2",
			Action: func(c *cli.Context) (err error) {
				err = godefer.Two()
				return
			},
		},
		{
			Name: "defer-3",
			Action: func(c *cli.Context) (err error) {
				err = godefer.Three()
				return
			},
		},
		{
			Name: "defer-4",
			Action: func(c *cli.Context) (err error) {
				err = godefer.Four()
				return
			},
		},
		{
			Name: "defer-5",
			Action: func(c *cli.Context) (err error) {
				err = godefer.Five()
				return
			},
		},
		{
			Name: "panic-1",
			Action: func(c *cli.Context) (err error) {
				err = gopanic.One()
				return
			},
		},
		{
			Name: "panic-2",
			Action: func(c *cli.Context) (err error) {
				err = gopanic.Two()
				return
			},
		},
		{
			Name: "recover-1",
			Action: func(c *cli.Context) (err error) {
				err = gorecover.One()
				return
			},
		},
		{
			Name: "summary-1",
			Action: func(c *cli.Context) (err error) {
				err = gosummay.One()
				return
			},
		},
		{
			Name: "db-1",
			Action: func(c *cli.Context) (err error) {
				err = db.One()
				return
			},
		},
		{
			Name: "channel-1",
			Action: func(c *cli.Context) (err error) {
				err = channel.One()
				return
			},
		},
		{
			Name: "channel-2",
			Action: func(c *cli.Context) (err error) {
				err = channel.Two()
				return
			},
		},
		{
			Name: "channel-3",
			Action: func(c *cli.Context) (err error) {
				err = channel.Three()
				return
			},
		},
		{
			Name: "channel-4",
			Action: func(c *cli.Context) (err error) {
				err = channel.Four()
				return
			},
		},
		{
			Name: "channel-5",
			Action: func(c *cli.Context) (err error) {
				err = channel.Five()
				return
			},
		},
		{
			Name: "channel-6",
			Action: func(c *cli.Context) (err error) {
				err = channel.Six()
				return
			},
		},
		{
			Name: "channel-7",
			Action: func(c *cli.Context) (err error) {
				err = channel.Seven()
				return
			},
		},
		{
			Name: "channel-8",
			Action: func(c *cli.Context) (err error) {
				err = channel.Eight()
				return
			},
		},
		{
			Name: "channel-9",
			Action: func(c *cli.Context) (err error) {
				err = channel.Nine()
				return
			},
		},
		{
			Name: "channel-10",
			Action: func(c *cli.Context) (err error) {
				err = channel.Ten()
				return
			},
		},
		{
			Name: "channel-11",
			Action: func(c *cli.Context) (err error) {
				err = channel.Eleven()
				return
			},
		},
		{
			Name: "channel-12",
			Action: func(c *cli.Context) (err error) {
				err = channel.Twelve()
				return
			},
		},
		{
			Name: "channel-13",
			Action: func(c *cli.Context) (err error) {
				err = channel.Thirteen()
				return
			},
		},
		{
			Name: "channel-14",
			Action: func(c *cli.Context) (err error) {
				err = channel.Fourteen()
				return
			},
		},
		{
			Name: "channel-15",
			Action: func(c *cli.Context) (err error) {
				err = channel.Fifteen()
				return
			},
		},
		{
			Name: "crawl-1",
			Action: func(c *cli.Context) (err error) {
				err = crawl.One()
				return
			},
		},
		{
			Name: "crawl-2",
			Action: func(c *cli.Context) (err error) {
				err = crawl.Two()
				return
			},
		},
		{
			Name: "sort",
			Action: func(c *cli.Context) (err error) {
				arr := []int{35, 33, 42, 10, 14, 19, 27, 44, 44}
				n := 4
				log.Println(arr)

				newArr := []int{}

				for _, i := range arr {
					if len(newArr) == 0 { // mảng rỗng
						//chèn vào đầu mảng
						newArr = append([]int{i}, newArr...)
					} else if i > newArr[0] { // lớn hơn phần tử đầu tiên
						//chèn vào đầu mảng
						newArr = append([]int{i}, newArr...)
					} else if i < newArr[len(newArr)-1] { // nhỏ hơn phần tử cuối cùng
						//chèn vào cuối mảng
						newArr = append(newArr, i)
					} else {
						index := sort.Search(len(newArr), func(j int) bool { return newArr[j] <= i })
						if arr[index] != i { // nếu phần tử tìm được bằng luôn thì bỏ qua
							tempArr := append([]int{}, newArr[:index]...)
							tempArr = append(tempArr, i)
							newArr = append(tempArr, newArr[index:]...)
						}
					}
					if len(newArr) > n {
						newArr = newArr[:n]
					}
				}
				log.Println(newArr)
				return
			},
		},
	}

	if len(os.Args) == 1 {
		os.Args = append(os.Args, "sort")
	}

	app.Run(os.Args)
}
