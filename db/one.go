package db

import (
	"crypto/tls"
	"log"
	"net"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type Contract struct {
	ID   int    `json:"id" bson:"_id,omitempty"`
	Name string `json:"name" bson:"name,omitempty"`
}

func One() (err error) {
	dialInfo := &mgo.DialInfo{
		Addrs:          []string{"cluster0-shard-00-00-khstw.gcp.mongodb.net:27017", "cluster0-shard-00-01-khstw.gcp.mongodb.net:27017", "cluster0-shard-00-02-khstw.gcp.mongodb.net:27017"},
		ReplicaSetName: "Cluster0-shard-0",
		Database:       "nordiccoder",
		Username:       "admin",
		Password:       "hjXuYzPbYhEg4oLi",
		Timeout:        5 * time.Second,
		Source:         "admin",
	}

	dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
		conn, err := tls.Dial("tcp", addr.String(), &tls.Config{})
		return conn, err
	}

	session, err := mgo.DialWithInfo(dialInfo)

	if err != nil {
		log.Printf("Lỗi kết nối: %v\n", err)
	} else {
		collection := session.DB("nordiccoder").C("contract")

		var contract Contract

		err = collection.Find(bson.M{"_id": 1}).One(&contract)

		if err != nil {
			panic(err)
		}

		log.Println(contract.Name)

		contract.Name = "Hợp đồng Sendo"

		err = collection.Update(bson.M{"_id": 1}, contract)

		if err != nil {
			panic(err)
		}

		var contract2 Contract
		contract2.Name = "Hợp đồng Tiki"

		var contract3 Contract
		contract3.ID = 1
		contract3.Name = "Hợp đồng Nordic"

		err = collection.Insert(contract2, contract3)
		if err != nil {
			panic(err)
		}
	}

	defer session.Close()

	return
}
