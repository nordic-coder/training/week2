package goroutine

import (
	"log"
)

func One() (err error) {

	for i := 0; i < 10; i++ {
		log.Println(i)
	}

	log.Println("=================")

	for i := 0; i < 10; i++ {
		log.Println(i)
	}

	return
}
