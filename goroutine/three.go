package goroutine

import (
	"log"
	"runtime"
)

func Three() (err error) {

	go func() {
		for i := 0; i < 10; i++ {
			log.Println(i)
		}
	}()

	go func() {
		for i := 0; i < 10; i++ {
			log.Println(i)
		}
	}()

	num := runtime.NumGoroutine()

	log.Printf("num: %v\n", num)

	return
}
