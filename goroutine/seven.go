package goroutine

import (
	"log"
)

func Seven() (err error) {

	wg.Add(1)

	go func() {
		for i := 0; i < 10; i++ {
			log.Println(i)
		}
		wg.Done()
	}()

	wg.Wait()

	log.Println("=================")

	// time.Sleep(time.Duration(1 * time.Second))
	return
}
