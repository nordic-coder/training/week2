package goroutine

import (
	"log"
	"sync"
)

var waitGroup sync.WaitGroup

// Four : Data race
func Four() (err error) {

	log.Println("Chạy trước") ///1

	waitGroup.Add(2)

	go func() {
		for i := 0; i < 10; i++ {
			log.Println(i)
		}
		waitGroup.Done()
	}()

	go func() {
		for i := 0; i < 10; i++ {
			log.Println(i)
		}
		waitGroup.Done()
	}()

	waitGroup.Wait()

	log.Println("Chạy sau") //3

	return
}
