package goroutine

import (
	"log"
	"time"
)

func Six() (err error) {

	go func() {
		for i := 0; i < 1000000; i++ {
			log.Println(i)
		}
	}()

	time.Sleep(1 * time.Millisecond)
	return
}
