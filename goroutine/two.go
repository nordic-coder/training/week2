package goroutine

import (
	"log"
)

func Two() (err error) {

	go func() {
		for i := 0; i < 10; i++ {
			log.Println(i)
		}
	}()

	log.Println("=================")

	go func() {
		for i := 0; i < 10; i++ {
			log.Println(i)
		}
	}()

	// time.Sleep(time.Duration(1 * time.Second))
	return
}
