package goroutine

import (
	"log"
	"sync"
)

var wg sync.WaitGroup

func Five() (err error) {

	log.Println("Chạy trước")

	wg.Add(2)

	go func() {
		for i := 0; i < 10; i++ {
			log.Println(i)
		}
		wg.Done()
	}()

	go func() {
		for i := 0; i < 10; i++ {
			log.Println(i)
		}
		wg.Done()
	}()

	wg.Wait()

	log.Println("Chạy sau")

	return
}
